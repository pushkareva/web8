 function setLocation(curLoc) {
    history.pushState(null, null, curLoc);
    return;
}

$('#myModal').on('hide.bs.modal', function (e) {
    if (location.hash == '#modal') window.history.back();
});

$(window).on('popstate', function (event) {
    if (event.state !== null) $('.modal').modal('hide');
});

$('.modal').on('shown.bs.modal', function () {
    document.getElementById('fio').value = localStorage.getItem('fio');
    document.getElementById('email').value = localStorage.getItem('email');
    document.getElementById('message').value = localStorage.getItem('message');
    if (localStorage.getItem('check') === "true") {
        $('#check').prop('checked', true);
    }
    else { $('#check').prop('checked', false); }
});

$('.modal').on('hidden.bs.modal', function () {
    localStorage.setItem('fio', $('#fio').val());
    localStorage.setItem('email', $('#email').val());
    localStorage.setItem('message', document.getElementById('message').value);
    localStorage.setItem('check', $('#check').is(':checked'));
});

function done() {
    document.getElementById('fio').value = "";
    document.getElementById('email').value = "";
    document.getElementById('message').value = "";
    $('#check').prop('checked', false);
    localStorage.clear();
};

$(document).ready(function () {
    $('#formData').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: 'https://api.slapform.com/pushkarevayulia@mail.ru',
            dataType: "json",
            method: 'POST',
            data: {
                name: $('#fio').val(),
                email: $('#email').val(),
                message: document.getElementById('message').value,
                check: $('#check').is(':checked'),
                slap_captcha: false
            },
            success: function (response) {
                console.log('Got data: ', response);
                if (response.meta.status == 'success') {
                    $('.modal').modal('hide');
                    alert("Успешно отправлено!");
                    done();
                } else if (response.meta.status == 'fail') {
                    $('.modal').modal('hide');
                    alert("Что-то пошло не так");
                    console.log('Submission failed with these errors: ', response.meta.errors);
                }
            }
        });
    });
});
